package operations;

import main.Counter;
import models.Memory;

public class Jump implements Operation {

	private int jumpto;

	/** Jumps to a certain place in the program queue. */
	public Jump(int jumpTo) {
		this.jumpto = jumpTo;
	}

	@Override
	public void action(Counter counter, Memory memory) {
		counter.jumpTo(jumpto);
		counter.decrease();
	}

	public String toString() {
		return "JMP " + jumpto;
	}
}
