package operations;

import main.Counter;
import models.Memory;
import models.Operand;

public class JumpEq extends Jump {

	private Operand a;
	private Operand b;
	private int jumpto;

	/** Jumps to a certain place in the queue if the two operands are equal. */
	public JumpEq(int jumpTo, Operand a, Operand b) {
		super(jumpTo);
		this.jumpto = jumpTo;
		this.a = a;
		this.b = b;
		// TODO Auto-generated constructor stub
	}

	public void action(Counter counter, Memory memory) {
		if (a.get(memory).getValue() == b.get(memory).getValue()) {
			super.action(counter, memory);
		} else {
			counter.decrease();
		}
	}

	public String toString() {
		return "JEQ " + jumpto + " " + a + b;
	}
}
