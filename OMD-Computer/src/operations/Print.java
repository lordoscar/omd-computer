package operations;

import main.Counter;
import models.Address;
import models.Memory;

public class Print implements Operation {

	private Address address;

	/** Prints the value currently in the memory at address. */
	public Print(Address address) {
		this.address = address;
	}

	@Override
	public void action(Counter counter, Memory memory) {
		System.out.println(memory.getWord(address.getAddress()));
		counter.decrease();
	}

	public String toString() {
		return "PRT " + address;
	}
}
