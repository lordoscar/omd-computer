package operations;

import main.Computer;
import main.Counter;
import models.Address;
import models.Memory;
import models.Word;

public class Copy implements Operation {

	private Word word;
	private Address address;
	
	/** Copies the word and place it at address */
	public Copy(Word word, Address address) {
		this.word = word;
		this.address = address;
		//place = counter.getCount() + 1;
	}

	@Override
	public void action(Counter counter, Memory memory) {
		// TODO Auto-generated method stub
		Computer.memory.insert(word, address);
		counter.decrease();
	}

	public String toString() {
		return "COP " + word.getValue() + " " + address.toString();
	}
}
