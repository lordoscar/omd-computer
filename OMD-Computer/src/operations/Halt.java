package operations;

import main.Counter;
import models.Memory;

public class Halt implements Operation {

	/** Puts the program count to -1 and thereby stops the program. */
	public Halt() {
		//place = counter.getCount() + 1;
	}

	@Override
	public void action(Counter counter, Memory memory) {
		// TODO Auto-generated method stub
		counter.setNegative();
	}

	public String toString() {
		return "HLT";
	}
}
