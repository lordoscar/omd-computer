package operations;

import main.Counter;
import models.Operand;
import models.Address;
import models.Word;
import models.Memory;

public abstract class BinOp implements Operation {

	Operand a;
	Operand b;
	Address address;

	public BinOp(Operand a, Operand b, Address address) {
		this.a = a;
		this.b = b;
		this.address = address;
	}

	protected abstract Word calc(Memory memory);

	public abstract String toString();

	public void action(Counter counter, Memory memory) {
		memory.insert(calc(memory), address);
		counter.decrease();
	}
}
