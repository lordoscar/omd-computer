package operations;

import models.Address;
import models.Memory;
import models.Operand;
import models.Word;

public class Mul extends BinOp {

	/**
	 * Multiplies the two operands and places the product in the memory at
	 * address.
	 */
	public Mul(Operand a, Operand b, Address address) {
		super(a, b, address);
		//place = counter.getCount() + 1;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Word calc(Memory memory) {
		// TODO Auto-generated method stub
		return a.get(memory).mul(b.get(memory));
	}

	public String toString() {
		return "MUL " + a + b + address;
	}
}
