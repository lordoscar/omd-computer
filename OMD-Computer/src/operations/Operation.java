package operations;

import main.Counter;
import models.Memory;

public interface Operation {
	public void action(Counter counter, Memory memory);
	
	public String toString();
}