package operations;

import models.Address;
import models.Memory;
import models.Operand;
import models.Word;

public class Add extends BinOp {
	/** Adds two operands and place the sum in memory at place address. */
	public Add(Operand a, Operand b, Address address) {
		super(a, b, address);
	}

	@Override
	protected Word calc(Memory memory) {
		return a.get(memory).add(b.get(memory));
	}

	public String toString() {
		return "ADD " + a + b + address;
	}
}
