package models;

public interface Word extends Operand {
	public Word get(Memory memory);
	
	public Number getValue();

	public String toString();
	
	public Word add(Word b);
	
	public Word mul(Word b);
	
}