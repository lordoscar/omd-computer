package models;

public class LongMemory implements Memory {

	private Word[] memory;

	public LongMemory(int capacity) {
		memory = new Word[capacity];
	}

	@Override
	public void insert(Word word, Address address) {
		memory[address.getAddress()] = word;
	}

	@Override
	public Word getWord(int address) {
		return memory[address];
	}
}
