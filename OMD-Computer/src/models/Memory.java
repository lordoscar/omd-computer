package models;

public interface Memory {
	public Word getWord(int address);

	public void insert(Word word, Address address);

}
