package models;

public interface Operand {	
	public Word get(Memory memory);

	public String toString();
}
