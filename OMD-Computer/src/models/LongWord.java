package models;

public class LongWord implements Word {

	long value;

	public LongWord(long word) {
		this.value = word;
	}

	@Override
	public Word get(Memory memory) {
		// TODO Auto-generated method stub
		return this;
	}
	
	public Number getValue(){
		return value;
	}
	
	public Word add(Word b){
		return new LongWord(value + b.getValue().longValue());
	}
	
	public Word mul(Word b){
		return new LongWord(value * b.getValue().longValue());
	}

	public String toString() {
		return Long.toString(value) + " ";
	}
}
