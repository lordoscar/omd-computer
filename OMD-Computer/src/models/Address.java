package models;

public class Address implements Operand {

	private int address;

	/** Represents an address in the memory. */
	public Address(int address) {
		this.address = address;
	}

	public int getAddress() {
		return address;
	}

	@Override
	public Word get(Memory memory) {
		return memory.getWord(address).get(memory);
	}

	public String toString() {
		return "[" + Integer.toString(address) + "] ";
	}
}
