package main;

public final class Counter {	
	
	private int count = -1;
	private int max = 0;
	

    public  Counter() {

    }
	
	public void increase() {
		count++;
		max++;
	}

	public void decrease() {
		count--;
	}

	public void setNegative() {
		count = -1;
	}

	public void jumpTo(int i) {
		count = max - i;
	}

	public int getCount() {
		return count;
	}
}
