package main;

import java.util.ArrayList;
import operations.Operation;

public class Program extends ArrayList<Operation> {

	private Counter counter;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Program() {
		super();
		counter = new Counter();
	}

	public boolean add(Operation op) {
		boolean added = super.add(op);
		if (added) {
			counter.increase();
		}
		return added;	
	}
	
	public Counter getCounter(){
		return counter;
	}
}
