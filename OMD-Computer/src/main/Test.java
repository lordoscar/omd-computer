package main;

import models.LongMemory;

public class Test {

	/** Test-program to test functionality of program */
	public static void main(String[] args) {
		Program factorial = new Factorial();
		System.out.println(factorial);
		Computer computer = new Computer(new LongMemory(1024));
		computer.load(factorial);
		computer.run();
	}
}