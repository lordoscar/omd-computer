package main;

import models.*;
import operations.*;

public class Factorial extends Program {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Counter counter;
	
	public Factorial() {
		counter = super.getCounter();
		Address n = new Address(0), fac = new Address(1);
		add(new Copy(new LongWord(5), n));
		add(new Copy(new LongWord(1), fac));
		add(new JumpEq(6, n, new LongWord(1)));
		add(new Mul(fac, n, fac));
		add(new Add(n, new LongWord(-1), n));
		add(new Jump(2));
		add(new Print(fac));
		add(new Halt());
	}
	
	public Counter getCounter(){
		return counter;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(Operation op : this){
			sb.append(this.indexOf(op) + " " + op.toString() + "\n");
		}
		return sb.toString();
	}
}