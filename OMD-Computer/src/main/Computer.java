package main;

import models.*;
import operations.*;

public class Computer {
	public static Memory memory;
	Program program;

	public Computer(Memory memory) {
		Computer.memory = memory;
	}

	public void load(Program program) {
		this.program = program;
	}

	public void run() {
		Counter counter = program.getCounter();
		int opcount = counter.getCount();
		while (counter.getCount() > -1) {
			int currentop = opcount - counter.getCount();
			Operation op = program.get(currentop);
			op.action(counter, memory);
			
		}
	}
}