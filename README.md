http://fileadmin.cs.lth.se/cs/Education/EDA061/projects/computer/index.html

Uppgifter:


1:  Klasser som finns: Computer, Program, Factorial, Memory,  LongMemory, Address, LongWord, Add, Mul, Copy, Halt, Print, Jump, JumpEq
    
Klasser som behöver skapas: 

Operations-interface för att samla alla operationer, 

En abstrakt klass BinOp för att hantera Add och Mul utan duplicerad kod, 

Word-interface till LongWord-klassen
    
2:  List

3:  Ett paket för operationerna och counter, ett paket för Memory, Word och ett paket Computer, Program, Factorial och Address.

4:  Används som interface för alla operationer, där alla har gemensamt metoden action().

5:  Aktuellt i BinOp, för klasserna Add och Mul som gör liknande saker, där enda skillnaden är att man använder addition/multiplikation.

6:  Operation delegerar ner arbetet via interfacet till t.ex. BinOp.

7:  Den sker i Add-klassen och returneras.

8:  Se bild 'sekvensdiagram.jpg'.



//Kod för sekvensdiagram:

title Add-exekvering

Factorial->+BinOp: action(counter, memory)
BinOp->+Add: Word calc(memory)
Add->+Address: Word a.get(memory)
Address->+LongWord: Word get(memory)
LongWord->LongWord: Word add(b.get())
LongWord->-Address:
Address->-Add:
Add->-BinOp:
BinOp->Factorial: memory.insert(word,address)

